# JetBrains License Server for Java

A license server for JetBrains products.

***
Thank ilanyu

NOTE: This is provided for educational purposes only. Please support genuine.
***
## Setup
Run:
```
cd /path/to/project
mvn compile 
mvn exec:java -Dexec.mainClass="com.vvvtimes.server.MainServer" 
```
default port is 8081.

## Support

IntelliJ IDEA>=7.0

ReSharper>=3.1

ReSharper>=Cpp 1.0

dotTrace>=5.5

dotMemory>=4.0

dotCover>=1.0

RubyMine>=1.0

PyCharm>=1.0

WebStorm>=1.0

PhpStorm>=1.0

AppCode>=1.0

CLion>=1.0